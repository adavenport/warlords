package utils

import (
	"context"
	"time"

	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// Check an error
func Check(e error) {
	if e != nil {
		panic(e)
	}
}

// Extracts the id from a gin context ptr
func IdParam(c *gin.Context) (primitive.ObjectID, error) {
	return primitive.ObjectIDFromHex(c.Param("id"))
}

// Context with 10 second timeout
func Context() (context.Context, context.CancelFunc) {
	return context.WithTimeout(context.Background(), 10*time.Second)
}
