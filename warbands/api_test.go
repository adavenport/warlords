package warbands

import (
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/goccy/go-json"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func (s *DbSuite) ApiRequest(method string, url string, body io.Reader) *httptest.ResponseRecorder {
	r := httptest.NewRecorder()
	req, _ := http.NewRequest(method, url, body)
	s.Router.ServeHTTP(r, req)
	return r
}

// Test finding all from api
func (s *DbSuite) TestApiAll() {
	r := s.ApiRequest("GET", "/api/wb/", nil)
	assert.Equal(s.T(), 200, r.Code)
	var actual []Warband
	json.Unmarshal(r.Body.Bytes(), &actual)
	exp, _ := FindAllWb()
	assert.Equal(s.T(), len(exp), len(actual))
	assert.Equal(s.T(), exp, actual)
}

// Test finding a warband
func (s *DbSuite) TestApiFind() {
	all, _ := FindAllWb()
	for _, exp := range all {
		url := "/api/wb/" + exp.ID.Hex()
		r := s.ApiRequest("GET", url, nil)
		var actual Warband
		json.Unmarshal(r.Body.Bytes(), &actual)
		assert.Equal(s.T(), exp.Name, actual.Name)
		assert.Equal(s.T(), exp, actual)
	}
}

// Test api delete
func (s *DbSuite) TestApiDelete() {
	var wb Warband
	r, _ := InsertWb(wb)
	wbs, _ := FindAllWb()
	eCount := len(wbs)

	id := r.InsertedID.(primitive.ObjectID).Hex()
	url := "/api/wb/" + id + "/delete"
	rec := s.ApiRequest("POST", url, nil)
	wbs, _ = FindAllWb()
	aCount := len(wbs)
	assert.Equal(s.T(), eCount, aCount+1)
	assert.Equal(s.T(), 200, rec.Code)

}

func TestApi(t *testing.T) {
	suite.Run(t, new(DbSuite))
}
