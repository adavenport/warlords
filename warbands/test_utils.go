package warbands

import (
	"context"
	"io/ioutil"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const TEST_DB string = "TESTING"

type DbSuite struct {
	suite.Suite
	Client  *mongo.Client
	Context context.Context
	Router  *gin.Engine
}

// Test Setup
func (s *DbSuite) SetupTest() {
	s.Context = context.Background()
	s.Client, _ = mongo.Connect(s.Context, options.Client().ApplyURI("mongodb://localhost:27017"))
	s.Router = gin.New()
	// Prevent spam by discarding output
	gin.DefaultWriter = ioutil.Discard
	SetupRoutes(s.Router)

	SetupDB(s.Client, TEST_DB)
	all, _ := FindAllWb()
	assert.Equal(s.T(), 0, len(all))

	// Some random words from /usr/share/dict/words
	words := []string{"laboratory", "dash", "becoming", "canopy", "wobble", "bing", "feudalism", "combinatorially", "futility", "prowler", "intonation", "brackets", "deform", "reservoir", "distortions", "risings", "leafy", "tablespoonfuls", "soothe", "symmetries", "consolidated", "segregation", "equivocal", "loan", "irritable"}

	for _, name := range words {
		var wb Warband
		wb.Name = name
		InsertWb(wb)
	}
}

// Delete all warbands from test database
func (s *DbSuite) TearDownTest() {

	all, _ := FindAllWb()
	for _, wb := range all {
		DeleteWarband(wb.ID)
	}
	defer s.Client.Disconnect(s.Context)
}
