package warbands

import (
	"os"
	"path"

	"gitlab.com/adavenport/warlords/utils"

	"github.com/goccy/go-json"
)

// Returns test data loaded from json files
func TestWbData() []Warband {
	var warbands []Warband
	gp := os.Getenv("GOPATH")
	dir := path.Join(gp, "src/gitlab.com/adavenport/warlords/json")
	files, err := os.ReadDir(dir)
	utils.Check(err)
	// Todo add concurency for larger data sets
	for _, f := range files {
		wb, err := LoadWarbandJson(path.Join(dir, f.Name()))
		if err == nil {
			warbands = append(warbands, wb)
		} else {
			panic(err)
		}
	}
	return warbands
}

// Loads a single warband from a json file
func LoadWarbandJson(filePath string) (Warband, error) {
	var wb Warband
	data, err := os.ReadFile(filePath)
	if err == nil {
		wb, err = ParseWarband(data)
	}
	return wb, err
}

// Unmarshals the json data for a wb
func ParseWarband(data []byte) (Warband, error) {
	var wb Warband
	err := json.Unmarshal(data, &wb)
	return wb, err
}
