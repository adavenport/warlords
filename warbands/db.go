package warbands

import (
	"log"

	"gitlab.com/adavenport/warlords/utils"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

var Client *mongo.Client
var Database *mongo.Database
var Collection *mongo.Collection

// Initialize the database connection
func SetupDB(client *mongo.Client, db string) {
	Client = client
	Database = client.Database(db)
	Collection = Database.Collection("warbands")

	wb, _ := FindAllWb()
	for _, w := range wb {
		DeleteWarband(w.ID)
	}
	//PopulateJson()
}

// Populate the database with json data
func PopulateJson() {
	json := TestWbData()
	for _, w := range json {
		_, err := InsertWb(w)
		if err != nil {
			log.Fatal(err)
		}
	}
}

// Insert a warband into the database
func InsertWb(wb Warband) (*mongo.InsertOneResult, error) {
	ctx, cancel := utils.Context()
	defer cancel()
	r, e := Collection.InsertOne(ctx, wb)
	return r, e
}

// Find a warband filtered by id
func FindWbById(id primitive.ObjectID) (Warband, error) {
	ctx, cancel := utils.Context()
	defer cancel()
	var w Warband
	var e error = nil
	defer cancel()
	result := Collection.FindOne(ctx, bson.M{"_id": id})
	e = result.Err()
	if e == nil {
		e = result.Decode(&w)
	}
	return w, e
}

// Find a warband filtered by bson
func FindWb(b bson.M) (Warband, error) {
	ctx, cancel := utils.Context()
	defer cancel()

	var w Warband
	result := Collection.FindOne(ctx, b)
	if result.Err() != nil {
		e := result.Decode(&w)
		if e != nil {
			panic(e)
		}
	}
	return w, result.Err()
}

// Find all warbands and return them in a slice
func FindAllWb() ([]Warband, error) {
	ctx, cancel := utils.Context()
	defer cancel()
	var wbs []Warband
	cur, err := Collection.Find(ctx, bson.D{})

	if err == nil {
		err = cur.All(ctx, &wbs)
	}
	return wbs, err
}

func UpdateWb(id primitive.ObjectID, wb Warband) error {
	ctx, cancel := utils.Context()
	defer cancel()
	_, e := Collection.ReplaceOne(ctx, bson.M{"_id": id}, wb)

	//_, e := Collection.UpdateByID(ctx, id, wb)
	return e
}

// Delete a warband by id
func DeleteWarband(id primitive.ObjectID) error {
	ctx, cancel := utils.Context()
	defer cancel()
	_, err := Collection.DeleteOne(ctx, bson.M{"_id": id})
	return err
}
