package warbands

import (
	"log"
	"net/http"
	"os"
	"path"

	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// Sets up the routes for warbands
func SetupRoutes(router *gin.Engine) {
	router.GET("/", index)
	router.GET("/chaos", chaos)
	wb := router.Group("/wb")
	{
		wb.GET(":id", view)
	}
	api := api.apiRoutes(router)

}

// Chaos
func chaos(c *gin.Context) {
	gp := os.Getenv("GOPATH")
	wb_chaos := path.Join(gp, "src/gitlab.com/adavenport/warlords/json", "chaos.json")
	w, err := LoadWarbandJson(wb_chaos)
	if err == nil {
		c.HTML(
			http.StatusOK,
			"view.html",
			gin.H{
				"title":   w.Name,
				"payload": w,
			},
		)
	} else {
		log.Println(err)
		c.HTML(
			http.StatusBadRequest,
			"404.html",
			gin.H{"message": "Warband not found."},
		)
	}
}

// Index route
func index(c *gin.Context) {
	wb, e := FindAllWb()
	if e == nil {
		c.HTML(
			http.StatusOK,
			"index.html",
			gin.H{
				"title":   "Warbands of Erewhon",
				"payload": wb,
			})
	}
}

// View wb route
func view(c *gin.Context) {
	var w Warband
	id, err := primitive.ObjectIDFromHex(c.Param("id"))
	if err == nil {
		w, err = FindWbById(id)
	}
	if err == nil {
		c.HTML(
			http.StatusOK,
			"view.html",
			gin.H{
				"title":   w.Name,
				"payload": w,
			},
		)
	} else {
		log.Println(err)
		c.HTML(
			http.StatusBadRequest,
			"404.html",
			gin.H{"message": "Warband not found."},
		)
	}
}
