package warbands

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/adavenport/warlords/utils"
	"go.mongodb.org/mongo-driver/bson"
)

func apiRotues(router *gin.Engine) *gin.RouterGroup {
	api := router.Group("/api/wb")
	{
		api.GET("/", apiAll)
		api.GET(":id", apiFind)
		api.POST(":id/delete", apiDelete)
	}
	return api
}

func apiAll(c *gin.Context) {
	all, err := FindAllWb()
	Collection.Find(c, bson.M{})
	if err == nil {
		c.JSON(http.StatusOK, all)
	}
}

func apiFind(c *gin.Context) {
	var w Warband
	id, err := utils.IdParam(c)
	if err == nil {
		w, err = FindWbById(id)
	}
	if err == nil {
		c.JSON(http.StatusOK, w)
	}
}

func apiDelete(c *gin.Context) {
	id, err := utils.IdParam(c)
	if err == nil {
		err = DeleteWarband(id)
	}
	if err == nil {
		c.String(http.StatusOK, "success")
	}

}

func apiUpdate(c *gin.Context) {

}
