package warbands

import "go.mongodb.org/mongo-driver/bson/primitive"

type Model struct {
	Desc    string `bson:"Desc"`
	Ag      string `bson:"Ag"`
	Acc     string `bson:"Acc"`
	Str     string `bson:"Str"`
	Res     string `bson:"Res"`
	Init    string `bson:"Init"`
	Co      string `bson:"Co"`
	Special string `bson:"Special"`
}

// TODO: remove calculated points
type Unit struct {
	Name        string   `bson:"Name"`
	Category    string   `bson:"Category"`
	BasePoints  int      `bson:"BasePoints"`
	BonusPoints int      `bson:"BonusPoints"`
	Models      []Model  `bson:"Models"`
	Options     []string `bson:"Options"`
}

// TODO: remove calculated points
type Warband struct {
	ID      primitive.ObjectID `bson:"_id,omitempty"`
	Name    string             `bson:"Name"`
	Author  string             `bson:"Author"`
	Faction string             `bson:"Faction"`
	Desc    string             `bson:"Desc"`
	Units   []Unit             `bson:"Units"`
	Rules   map[string]string  `bson:"Rules"`
}

func (u Unit) Points() int {
	return u.BasePoints + u.BonusPoints
}

func (w Warband) Points() int {
	points := 0
	for _, u := range w.Units {
		points += u.Points()
	}
	return points
}
