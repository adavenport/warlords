package warbands

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func (s *DbSuite) TestCreate() {
	wb := TestWbData()[0]

	r, e := InsertWb(wb)
	assert.Equal(s.T(), e, nil)
	wb.ID = r.InsertedID.(primitive.ObjectID)

	inserted, e := FindWbById(wb.ID)
	assert.Equal(s.T(), e, nil)
	assert.Equal(s.T(), wb, inserted)

	e = DeleteWarband(wb.ID)
	assert.Equal(s.T(), e, nil)
}

func (s *DbSuite) TestFindall() {
	all, e := FindAllWb()
	assert.Equal(s.T(), e, nil)
	assert.NotEqual(s.T(), 0, len(all))
}

func (s *DbSuite) TestUpdate() {
	for i := 0; i < 10; i++ {
		wb := Warband{
			Name: "Test" + fmt.Sprintf("%d", i),
		}
		r, _ := InsertWb(wb)

		id := r.InsertedID.(primitive.ObjectID)
		wb.Name = "updated" + fmt.Sprintf("%d", i)
		UpdateWb(id, wb)
		exp := wb
		exp.ID = id
		actual, _ := FindWbById(id)
		assert.Equal(s.T(), exp.Name, actual.Name)
		assert.Equal(s.T(), exp, actual)
	}
}

func (s *DbSuite) TestDelete() {
	for i := 0; i < 10; i++ {
		wb := Warband{
			Name: "Test" + fmt.Sprintf("%d", i),
		}
		r, _ := InsertWb(wb)
		id := r.InsertedID.(primitive.ObjectID)
		e := DeleteWarband(id)
		assert.Equal(s.T(), nil, e)
	}

}

func TestWarbands(t *testing.T) {
	suite.Run(t, new(DbSuite))
}
